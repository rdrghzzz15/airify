from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import View
from .models import Genre
from django.urls import reverse
from django.shortcuts import redirect, get_object_or_404
# Create your views here.


class HomeView(TemplateView):
    template_name = 'music_polls/home.html'

    def get(self, request, *args, **kwargs):
        print('dajdsja')
        context = self.get_context_data(request)
        return self.render_to_response(context)

    def get_context_data(self, request, **kwargs):
        print('sadsad')
        context = super(HomeView, self).get_context_data(**kwargs)
        context['genres'] = Genre.objects.all()

        top_genre = None
        top_votes = 0

        for genre in Genre.objects.all():
            if genre.votes > top_votes:
                top_genre = genre
                top_votes = genre.votes

        context['top_genre'] = top_genre

        print(Genre.objects.all())
        return context

    def post(self, request, *args, **kwargs):
        if 'votes' in request.POST:
            genre_pk = request.POST.get('votes', 0)
            genre = get_object_or_404(Genre, pk=genre_pk)
            genre.votes += 1
            genre.save()
        return redirect(reverse('home'))
