from django.apps import AppConfig


class MusicPollsConfig(AppConfig):
    name = 'music_polls'
