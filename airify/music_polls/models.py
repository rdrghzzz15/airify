from django.db import models

# Create your models here.


class Genre(models.Model):
    genre_url = models.CharField(max_length=255, blank=False)
    genre_icon = models.CharField(max_length=255, blank=False)
    genre_id = models.CharField(max_length=255, blank=False)
    name = models.CharField(max_length=255, blank=False)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return ('{}:{}'.format(self.genre_id, self.name))
